Update Item in Listview in VB.net
-kitchanismo

# instructions

*add as reference getIndex.dll
  Go to Project > add reference > browse the dll

*import getIndex.dll in namespace(Top most)
  Imports kitchanismo

*instantiate Index
  Dim index As New Index

*call and assign function getIndex with parameters of ListView, unique value, index in Listview as Optional
  Dim i as integer = index.getIndex(<listview>, <value>, [index])

*update Listview by item
   <listview>.Items(i).SubItems(<column index>).Text = <value>

# sample codes

'import getIndex.dll in namespace
Imports kitchanismo

'instantiate index from kitchanismo
Dim index As New Index

'declaration of variables
Dim i As Integer
Dim prodcategory As String
Dim prodname As String
Dim id As Integer
Dim prodprice As Integer
Dim prodstock As Integer
Dim enterQty As Integer
Dim newStock As Integer
Dim subtotal As Double

Private Sub lvProducts_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvProducts.MouseDoubleClick
        
        'assign data from listview to variables
        With lvProducts.SelectedItems(0)
            id = .SubItems(0).Text
            prodcategory = .SubItems(1).Text
            prodname = .SubItems(2).Text
            prodprice = .SubItems(3).Text
            prodstock = .SubItems(4).Text
        End With

        'get qty order from the user and assign to enterQty
        enterQty = Val(InputBox("Product name: " & prodname & vbNewLine & "Product price: " & prodprice, "Enter Quantity".ToUpper, 1))
        
	'if user enter nothing then exit
	If enterQty = Nothing Then
            Exit Sub
        End If

        'if user enter qty greater than stock then prompt and exit 
        If enterQty > prodstock Then
            MsgBox("There is no enough number of Stock!", MsgBoxStyle.Critical, "Error")
            Exit Sub
        End If

        'compute subtotal
        subtotal = enterQty * prodprice

        *call and assign function getIndex with parameters of ListView and unique value
        i = index.getIndex(lvorder, prodname)

        'if i is greater than to -1 then update qty and subtotal else add new item to order list
        If i > -1 Then
	    
            'update qty and subtotal in Listview
            lvorder.Items(i).SubItems(2).Text += enterQty
            lvorder.Items(i).SubItems(3).Text += subtotal
        Else

            'add item in Listview
            With lvorder.Items.Add(prodname)
                .SubItems.Add(prodprice)
                .SubItems.Add(enterQty)
                .SubItems.Add(subtotal)
            End With
        End If

        'update total, change & no of items
        lbltotal.Text = ComputeCollumn(3)
        lblitems.Text = ComputeCollumn(2)
        lblchange.Text = ComputeChange(tbcash.Text, lbltotal.Text)

        'subtract qty stock to qty ordered
        newStock = prodstock - enterQty

	'create this function to update stock with new stock into database by product name
        updateStock(newStock, prodname)

End Sub